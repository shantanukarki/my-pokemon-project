import { useState, useEffect } from "react";

import getData from "../API/apiget";

const initialState = {
  count: undefined,
  next: undefined,
  previous: undefined,
  results: [],
};
const useGetData = () => {
  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [next, setNext] = useState(undefined);
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(20);
  // for initial render and adding next pages
  const myData = async (start, show, click) => {
    try {
      setError(false);
      setLoading(true);
      var myData = await getData(start, show, click);
      setData(myData);
      setLoading(false);
    } catch (error) {
      setError(true);
    }
  };
  const getNewData = async (start, show, fetch) => {
    try {
      setError(false);
      setLoading(true);
      var nextData = await getData(start, show, fetch);
      setData(
        nextData
        // (prevData) => {
        // var newData = prevData;
        // newData.next = nextData.next;
        // newData.previous = nextData.previous;
        // newData.results = [...prevData.results, ...nextData.results];
        // return newData;
        //}
      );
      setLoading(false);
    } catch (error) {
      setError(false);
    }
  };
  useEffect(() => {
    next ? getNewData(offset, limit, next) : myData(offset, limit, undefined);
  }, [next, offset, limit]);

  //to change pages
  // useEffect(()=>{
  //     setLoading(true)
  //     var newData = getNewData(next)
  //     setData(newData)
  //     setLoading(false)
  // },[next])

  return {
    data,
    loading,
    error,
    next,
    setNext,
    setLoading,
    offset,
    setOffset,
    limit,
    setLimit,
  };
};

export default useGetData;
