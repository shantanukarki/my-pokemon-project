import { useState, useCallback, useEffect } from "react/cjs/react.development";
import getSingleData from "../API/getSingleData";

const useGetSingle = () => {
  const [data, setData] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [type, setType] = useState(undefined);
  const [pokemonName, setPokemonName] = useState(undefined);

  const getData = async (getType, getName) => {
    try {
      setError(false);
      setLoading(true);
      var initialData = await getSingleData(getType, getName);
      setData(initialData);
      setLoading(false);
    } catch (error) {
      setError(true);
    }
  };
  useEffect(() => {
    setLoading(true);
    type && pokemonName && getData(type, pokemonName);
    setLoading(false);
  }, [type, pokemonName]);

  return { data, loading, error, setType, setPokemonName };
};

export default useGetSingle;
