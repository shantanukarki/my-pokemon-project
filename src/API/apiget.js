const baseURL = "https://pokeapi.co/api/v2/";

const getData = async (start, show, next) => {
  console.log(start);
  var rawData = !next
    ? await fetch(`${baseURL}pokemon?offset=${start}&limit=${show}`)
    : await fetch(`${next}`);
  var response = await rawData.json();
  return response;
};

export default getData;
