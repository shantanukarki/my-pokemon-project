const baseURL = "https://pokeapi.co/api/v2/";

const getSingleData = async (type, name) => {
  try {
    var rawData = await fetch(`${baseURL}${type}/${name}`);
    var response = await rawData.json();
    return response;
  } catch (error) {
    console.log(error);
  }
};

export default getSingleData;
