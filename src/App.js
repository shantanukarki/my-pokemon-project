import React from "react";
//router
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
//import Components
import FrontPage from "./Pages/FrontPage";
import Pokemon from "./Pages/singlePokemon";
import UserProvider from "./context";
const App = () => (
  <Router>
    <Routes>
      <Route path="/" element={<FrontPage />} />
      <Route path="/pokemon/:pokemonName" element={<Pokemon />} />
    </Routes>
  </Router>
);

export default App;
