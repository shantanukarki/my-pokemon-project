import React, { useState, useEffect } from "react";

import useGetData from "../hooks/useGetData";

import Banner from "../components/Banner";
import Listing from "../components/Listing";
const FrontPage = () => {
  const { data, loading, error, setNext, offset, setOffset, limit, setLimit } =
    useGetData();
  console.log(data.count);
  var total = data.count;
  var half = Math.floor(total / 2);
  console.log(half);
  const [show, setShow] = useState(limit);
  const [startfrom, setStartfrom] = useState(offset);
  const handleClick = (link) => {
    setNext(link);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setOffset(startfrom);
    setLimit(show);
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    name === "show" && setShow(value);
    name === "startfrom" && setStartfrom(value);
  };
  if (error) return <h1>Sorry we could not find your page</h1>;
  if (loading) return <h1>Loading ....</h1>;
  return (
    <>
      <div className="banner">
        <Banner />
      </div>
      <div className="filter">
        <form onSubmit={handleSubmit}>
          <label>Input How many you want to show : </label>
          <input
            type="number"
            name="show"
            onChange={handleChange}
            placeholder={limit}
            value={show}
            min="10"
            max={`${half}`}
          />
          <label>From Where do you want to start : </label>
          <input
            type="number"
            name="startfrom"
            onChange={handleChange}
            value={startfrom}
            placeholder={offset}
            min="0"
            max={`${total - show}`}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
      <div className="pokemon-list">
        {data.results.length > 0 && (
          <div>
            {data.results.map((pokemon, index) => (
              <Listing key={index} pokemon={pokemon} />
            ))}
          </div>
        )}
      </div>
      <div className="buttons">
        <div className={`${data.previous ? "show" : "hide"} prev-button`}>
          <button onClick={() => handleClick(data.prev)}>Prev</button>
        </div>
        <div className={`${data.next ? "show" : "hide"} next-button`}>
          <button onClick={() => handleClick(data.next)}>Next</button>
        </div>
      </div>
    </>
  );
};

export default FrontPage;
