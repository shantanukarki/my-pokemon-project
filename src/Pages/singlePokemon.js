import React, { useEffect } from "react";

import useGetSingle from "../hooks/useGetSingleList";
import { useParams } from "react-router-dom";

const Pokemon = () => {
  const { data, loading, error, setType, setPokemonName } = useGetSingle();
  const { pokemonName } = useParams();
  useEffect(() => {
    setType("pokemon");
    setPokemonName(pokemonName);
  });
  if (loading) return <h1>Loading ...</h1>;
  if (error) return <h1>Not Found</h1>;
  return (
    <>
      {data ? (
        <div>
          <img src={data.sprites.back_default} alt={pokemonName} />
          <h1>{pokemonName}</h1>
          <p>height: {data.height} Feet</p>
        </div>
      ) : (
        <h1>Loading ...</h1>
      )}
    </>
  );
};

export default Pokemon;
