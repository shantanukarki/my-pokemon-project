import React from "react";
import { useEffect, useState } from "react/cjs/react.development";
import useGetData from "../hooks/useGetData";

import { Link } from "react-router-dom";
const Listing = ({ pokemon }) => {
  const { setLoading } = useGetData();
  const [state, setState] = useState(undefined);
  const pokemonData = async () => {
    setLoading(true);
    var rawData = await fetch(
      `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
    );
    var response = await rawData.json();
    setState(response);
    setLoading(false);
  };
  useEffect(() => {
    pokemonData();
  }, []);
  return (
    <>
      <div className="pokemon">
        <Link to={`/pokemon/${pokemon.name}`}>
          {state ? (
            <img src={state.sprites.front_default} alt={pokemon.name}></img>
          ) : (
            <p>Loading ...</p>
          )}
          <p>{pokemon.name}</p>
        </Link>
      </div>
    </>
  );
};

export default Listing;
